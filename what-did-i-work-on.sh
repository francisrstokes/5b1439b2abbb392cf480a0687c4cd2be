#!/bin/bash

# Default value for days
DAYS=7

# Check if a custom number of days is provided as an argument
if [ "$1" ]; then
  DAYS=$1
fi

# Function to get the git commits for a repository
get_commits() {
  local repo=$1

  # Check if it's a valid git repository
  if [ -d "$repo/.git" ]; then
    cd "$repo" || exit
    # Get the commits made in the last $DAYS days
    commits=$(git log --since="$DAYS days ago" --pretty=format:"%ad: %s" --date=short)
    if [ -n "$commits" ]; then
      echo "=== $repo ==="
      echo "$commits"
      echo ""
    fi
    cd ..
  fi
}

# Iterate over all directories in the current directory
for dir in */; do
  if [ -d "$dir" ]; then
    get_commits "$dir"
  fi
done